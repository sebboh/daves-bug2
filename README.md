What is this all about?
=======================

https://debbugs.gnu.org/cgi/bugreport.cgi?bug=25550

TODO: paste all that in here.  Get the unicode right for browsers.


What should we do about that?
=============================

We should post on the glibc mailing list...

*However*... it will be more fun to solve it ourselves live at the FSF
offices at LibrePlanet 2017!

The Game Plan
=============

I think we're going to try to build `glibc` and then use `LD_PRELOAD` to
invoke `uniq`, and debug it in that manner.  Using GDB or something.
